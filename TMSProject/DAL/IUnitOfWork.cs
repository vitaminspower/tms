﻿using DAL.Repository;

namespace DAL
{
    public interface IUnitOfWork
    {

        UserRepository Users { get; }

        AccountRepository Accounts { get; }

        IncomingTransactionRepository IncomingTransactions { get; }        
        OutcomingTransactionRepository OutcomingTransactions { get; }

        EmployeeRepository Employee { get; }

        void SaveChanges();
    }
}
