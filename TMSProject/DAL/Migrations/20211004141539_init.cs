﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    HomeAdress = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EmailAdress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RegistrationTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    IsVerified = table.Column<bool>(type: "bit", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserPersonalAccount",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Currency = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPersonalAccount", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPersonalAccount_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IncomingTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiverBankAccountId = table.Column<int>(type: "int", nullable: false),
                    SenderBankAccountId = table.Column<int>(type: "int", nullable: false),
                    Source = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Currenct = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncomingTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncomingTransaction_UserPersonalAccount_ReceiverBankAccountId",
                        column: x => x.ReceiverBankAccountId,
                        principalTable: "UserPersonalAccount",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_IncomingTransaction_UserPersonalAccount_SenderBankAccountId",
                        column: x => x.SenderBankAccountId,
                        principalTable: "UserPersonalAccount",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OutcomingTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiverBankAccountId = table.Column<int>(type: "int", nullable: false),
                    SenderBankAccountId = table.Column<int>(type: "int", nullable: false),
                    Source = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Currency = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    UserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutcomingTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OutcomingTransaction_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OutcomingTransaction_UserPersonalAccount_ReceiverBankAccountId",
                        column: x => x.ReceiverBankAccountId,
                        principalTable: "UserPersonalAccount",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OutcomingTransaction_UserPersonalAccount_SenderBankAccountId",
                        column: x => x.SenderBankAccountId,
                        principalTable: "UserPersonalAccount",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "DateOfBirth", "EmailAdress", "HomeAdress", "IsVerified", "Name", "PhoneNumber", "Surname" },
                values: new object[] { 1, new DateTime(2021, 10, 4, 14, 15, 38, 994, DateTimeKind.Utc).AddTicks(2153), "Email@email.com", "adress", true, "Andrew", "123456789", "Watson" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "DateOfBirth", "EmailAdress", "HomeAdress", "IsVerified", "Name", "PhoneNumber", "Surname" },
                values: new object[] { 2, new DateTime(2021, 10, 4, 14, 15, 38, 994, DateTimeKind.Utc).AddTicks(4672), "another@email.com", "same adress", true, "John", "123456789", "Smith" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "DateOfBirth", "EmailAdress", "HomeAdress", "IsVerified", "Name", "PhoneNumber", "Surname" },
                values: new object[] { 3, new DateTime(2021, 10, 4, 14, 15, 38, 994, DateTimeKind.Utc).AddTicks(4680), "another@email.com", "Texas", true, "Walter", "123456789", "White" });

            migrationBuilder.InsertData(
                table: "UserPersonalAccount",
                columns: new[] { "Id", "Currency", "UserId" },
                values: new object[,]
                {
                    { 1, 0, 1 },
                    { 2, 1, 1 },
                    { 3, 0, 2 },
                    { 4, 1, 2 },
                    { 5, 0, 3 },
                    { 6, 1, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_IncomingTransaction_ReceiverBankAccountId",
                table: "IncomingTransaction",
                column: "ReceiverBankAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_IncomingTransaction_SenderBankAccountId",
                table: "IncomingTransaction",
                column: "SenderBankAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_OutcomingTransaction_ReceiverBankAccountId",
                table: "OutcomingTransaction",
                column: "ReceiverBankAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_OutcomingTransaction_SenderBankAccountId",
                table: "OutcomingTransaction",
                column: "SenderBankAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_OutcomingTransaction_UserId",
                table: "OutcomingTransaction",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPersonalAccount_UserId",
                table: "UserPersonalAccount",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncomingTransaction");

            migrationBuilder.DropTable(
                name: "OutcomingTransaction");

            migrationBuilder.DropTable(
                name: "UserPersonalAccount");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
