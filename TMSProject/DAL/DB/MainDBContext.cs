﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using DAL.Model;

namespace DAL.DB
{
    public class MainDBContext : DbContext
    {
        public MainDBContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Employee>(e =>
            {
                e.Property(x => x.Id)
                .IsRequired();
            });

            modelBuilder.Entity<User>(e =>
           {
               e.Property(x => x.IsVerified)
               .HasDefaultValue(false);            

               e.Property(x => x.RegistrationTime)
               .HasDefaultValueSql("GETDATE()");
           });

            modelBuilder.Entity<OutcomingTransaction>(e =>
            { 
                e.Property(x => x.Date)
                .HasDefaultValueSql("GETDATE()");

               e.HasOne(x => x.SenderAccount)
               .WithMany(x => x.OutComingTransactions)
               .HasForeignKey(x => x.SenderBankAccountId)
               .OnDelete(DeleteBehavior.NoAction);

            });

            modelBuilder.Entity<IncomingTransaction>(e =>
            {
                e.Property(x => x.Date)
                .HasDefaultValueSql("GETDATE()");

                e.HasOne(x => x.ReceiverAccount)
                .WithMany(x => x.IncomingTransactions)
                .HasForeignKey(x => x.ReceiverBankAccountId)
                .OnDelete(DeleteBehavior.NoAction);
            });

            #region seed Users

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Name = "Andrew",
                    Surname = "Watson",
                    EmailAdress = "Email@email.com",
                    DateOfBirth = DateTime.UtcNow,
                    HomeAdress = "adress",
                    PhoneNumber = "123456789",
                    IsVerified = true                   
                },

                new User
                {
                    Id = 2,
                    Name = "John",
                    Surname = "Smith",
                    EmailAdress = "another@email.com",
                    DateOfBirth = DateTime.UtcNow,
                    HomeAdress = "same adress",
                    PhoneNumber = "123456789",
                    IsVerified = true
                },
                new User
                {
                    Id = 3,
                    Name = "Walter",
                    Surname = "White",
                    EmailAdress = "another@email.com",
                    DateOfBirth = DateTime.UtcNow,
                    HomeAdress = "Texas",
                    PhoneNumber = "123456789",
                    IsVerified = true
                }
                );

            #endregion

            #region seed Bank Accounts

            modelBuilder.Entity<UserPersonalAccount>().HasData(
                new UserPersonalAccount { Id = 1, UserId = 1, Currency = DAL.Model.Enum.Currency.BYR },
                new UserPersonalAccount { Id = 2, UserId = 1, Currency = DAL.Model.Enum.Currency.USD },
                new UserPersonalAccount { Id = 3, UserId = 2, Currency = DAL.Model.Enum.Currency.BYR },
                new UserPersonalAccount { Id = 4, UserId = 2, Currency = DAL.Model.Enum.Currency.USD },
                new UserPersonalAccount { Id = 5, UserId = 3, Currency = DAL.Model.Enum.Currency.BYR },
                new UserPersonalAccount { Id = 6, UserId = 3, Currency = DAL.Model.Enum.Currency.USD }
              );

            #endregion

        }
    }
}
