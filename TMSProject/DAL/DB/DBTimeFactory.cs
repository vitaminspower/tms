﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;


namespace DAL.DB
{
    public class DBTimeFactory : IDesignTimeDbContextFactory<MainDBContext>
    {
        public MainDBContext CreateDbContext(string[] args)
        {
            var config = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                .Build();

            var connectionString = config.GetValue<string>("base");

            var optionsBuilder = new DbContextOptionsBuilder<MainDBContext>();
            optionsBuilder.UseSqlServer(connectionString);

            return new MainDBContext(optionsBuilder.Options);
           
        }
    }


}

