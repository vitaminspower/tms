﻿using DAL.DB;
using DAL.Repository;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly MainDBContext _context;


        private UserRepository _users;
        public UserRepository Users => _users ??= new UserRepository(_context);


        private AccountRepository _accounts;
        public AccountRepository Accounts => _accounts ??= new AccountRepository(_context);


        private IncomingTransactionRepository _incomingTransactions;
        public IncomingTransactionRepository IncomingTransactions => _incomingTransactions ??= new IncomingTransactionRepository(_context);

        private OutcomingTransactionRepository _outcomingTransactions;
        public OutcomingTransactionRepository OutcomingTransactions => _outcomingTransactions ??= new OutcomingTransactionRepository(_context);


        private EmployeeRepository _employees;
        public EmployeeRepository Employee => _employees ??= new EmployeeRepository(_context);

        public UnitOfWork(MainDBContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
