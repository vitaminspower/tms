﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.DB;

namespace DAL.Repository
{
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository(MainDBContext context)
            : base(context)
        {

        }

        public override void Delete(User user)
        {
            _context.Remove(user);
        }
    }
}
