﻿using DAL.DB;
using DAL.Model;

namespace DAL.Repository
{
    public class IncomingTransactionRepository : GenericRepository<IncomingTransaction>
    {
        public IncomingTransactionRepository(MainDBContext context)
        : base(context)
        {

        }

        public override void Delete(IncomingTransaction tr)
        {
            _context.Remove(tr);
        }


    }
}
