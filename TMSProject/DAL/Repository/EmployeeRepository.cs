﻿using DAL.DB;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{   

    public class EmployeeRepository : GenericRepository<Employee>
    {
        public EmployeeRepository(MainDBContext context)
        : base(context)
        {

        }

        public override void Delete(Employee tr)
        {
            _context.Remove(tr);
        }


    }
}
