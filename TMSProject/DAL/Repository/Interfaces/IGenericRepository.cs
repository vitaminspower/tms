﻿using DAL.Model;
using System.Linq;

namespace DAL.Repository.Interfaces
{
    public interface IGenericRepository<T>
        where T : class
    {
        public T Read(int id);
        public IQueryable<T> ReadAll();
        public T Update(T user); 
        void Delete(T entity);
    }
}
