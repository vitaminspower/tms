﻿using DAL.DB;
using DAL.Repository.Interfaces;
using System.Linq;

namespace DAL.Repository
{
    public class GenericRepository<T>
        : IGenericRepository<T>
        where T: class
    {
        protected readonly MainDBContext _context;
        public GenericRepository(MainDBContext context)
        {
            _context = context;
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        /// <summary>
        /// uses _context.Set<T>();
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }

        public T Update(T entity)
        {
            return _context.Update<T>(entity).Entity;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove<T>(entity);
        }

        public void Add(T user)
        {
            _context.Add(user);
        }

    }
}
