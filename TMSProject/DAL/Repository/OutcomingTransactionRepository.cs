﻿using DAL.DB;
using DAL.Model;

namespace DAL.Repository
{
    public class OutcomingTransactionRepository : GenericRepository<OutcomingTransaction>
    {
        public OutcomingTransactionRepository(MainDBContext context)
        : base(context)
        {

        }

        public override void Delete(OutcomingTransaction tr)
        {
            _context.Remove(tr);
        }


    }
}
