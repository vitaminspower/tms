﻿using DAL.DB;
using DAL.Model;

namespace DAL.Repository
{
    public class AccountRepository : GenericRepository<UserPersonalAccount>
    {
        public AccountRepository(MainDBContext context)
        : base(context)
        {

        }

        public override void Delete(UserPersonalAccount acc)
        {
            _context.Remove(acc);
        }


    }
}
