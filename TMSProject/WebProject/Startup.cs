using BLL.interfaces;
using BLL.services;
using DAL;
using DAL.DB;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
//using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IO;
using WebProject.ModelBinders;

namespace WebProject
{


    //viewBag.pageCount
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddControllersWithViews(opts =>
            {
               // opts.ModelBinderProviders.Insert(0, new CustomBinderProvider());
            });

            #region connection string

            var config = new ConfigurationBuilder()
                    .SetBasePath(Directory
                    .GetCurrentDirectory())
                    .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                    .Build();

            var connectionString = config.GetValue<string>("base");

            #endregion

            services.AddDbContext<MainDBContext>(options =>
                     options.UseSqlServer(connectionString));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IEmployeeService, EmployeeService>();


            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options => //CookieAuthenticationOptions
                    {
                     options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                    });

            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new CustomViewLocationExpander());
            }
            );

        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

#region sql helper
// dotnet ef migrations add mg2 --startup-project WebProject --project DAL --verbose
// dotnet ef database update --startup-project WebProject --project DAL --verbose
//@"Data Source=.\SQLEXPRESS;Initial Catalog=MainDb;Integrated Security=True"
//seed database
//cdc log
//efcorelogger
//
#endregion