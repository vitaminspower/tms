#pragma checksum "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2bfb02012fd47bce6e07a6ce602f3f7014d075a2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_PersonalAccounts_GetAllUserAccs), @"mvc.1.0.view", @"/Views/PersonalAccounts/GetAllUserAccs.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Projects\1\TMSProject\WebProject\Views\_ViewImports.cshtml"
using WebProject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Projects\1\TMSProject\WebProject\Views\_ViewImports.cshtml"
using WebProject.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Projects\1\TMSProject\WebProject\Views\_ViewImports.cshtml"
using WebProject.HTMLExtensions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
using DAL.Model;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2bfb02012fd47bce6e07a6ce602f3f7014d075a2", @"/Views/PersonalAccounts/GetAllUserAccs.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"44733e80f88b0f9aaba39c42cdc6d40adfec462a", @"/Views/_ViewImports.cshtml")]
    public class Views_PersonalAccounts_GetAllUserAccs : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<DAL.Model.UserPersonalAccount>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<div class=""text-center"">

    <table class=""table"">
        <tbody class=""btn-dark"">
            <tr>
                <th>account ID</th>
                <th>state</th>
                <th>operation</th>
                <th>incoming transactions</th>
                <th>outcoming transactions</th>
            </tr>
        </tbody>

");
#nullable restore
#line 17 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
         foreach (var acc in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 20 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
               Write(acc.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 21 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
               Write(acc.Amount);

#line default
#line hidden
#nullable disable
            WriteLiteral(" $</td>\r\n\r\n                <td>\r\n                    ");
#nullable restore
#line 24 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
               Write(Html.ActionLink(
                       "Transfer",
                       "AddTransaction",
                       "Transactions",
                          new WebProject.Models.dto.TransactionDto() { SenderId = acc.Id },
                          new { type = "button", @class = " btn btn-secondary" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n\r\n                <td>\r\n                    ");
#nullable restore
#line 33 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
               Write(Html.ActionLink("Open history", "GetAccountIncomingTransactionsHistory", "Transactions",
                         new { id = acc.Id },
                         new { type = "button", @class = "btn btn-secondary" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 38 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
               Write(Html.ActionLink("Open history", "GetAccountOutcomingTransactionsHistory", "Transactions",
                         new { id = acc.Id },
                         new { type = "button", @class = "btn btn-secondary" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n\r\n            </tr>\r\n");
#nullable restore
#line 44 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </table>\r\n\r\n    ");
#nullable restore
#line 47 "D:\Projects\1\TMSProject\WebProject\Views\PersonalAccounts\GetAllUserAccs.cshtml"
Write(Html.ActionLink("open new account", "AddNewAccountPage", "PersonalAccounts", new WebProject.Models.dto.PersonalAccountDto() { UserId = Model.First().UserId }, new { type = "button", @class = "btn btn-dark" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<DAL.Model.UserPersonalAccount>> Html { get; private set; }
    }
}
#pragma warning restore 1591
