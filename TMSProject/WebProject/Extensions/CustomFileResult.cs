﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using Microsoft.AspNetCore;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace WebProject.Extensions
{
    public class CustomFileResult : IActionResult
    {
        public CustomFileResult(object _obj)
        {
            file = _obj;
        }

        public string fileName = "test.txt";
        public object file;

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var json = JsonSerializer.Serialize(file);
            var bytes = Encoding.UTF8.GetBytes(json);

            var a = new MediaTypeHeaderValue("application/octet-stream");

            var res = new FileContentResult(bytes, a)
            {
                FileDownloadName = fileName
            };
            context.HttpContext.Request.Headers.Add("Content-Disposition", "attachment");

            await res.ExecuteResultAsync(context);
        }
    }


}
