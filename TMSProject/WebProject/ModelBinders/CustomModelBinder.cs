﻿//using Microsoft.AspNetCore.Mvc.ModelBinding;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using WebProject.Controllers;

//namespace WebProject.ModelBinders
//{
//    public class CustomModelBinder : IModelBinder
//    {
//        private readonly IModelBinder fallbackBinder;
//        public CustomModelBinder(IModelBinder fallbackBinder)
//        {
//            this.fallbackBinder = fallbackBinder;
//        }

//        public Task BindModelAsync(ModelBindingContext bindingContext)
//        {           
//            if (bindingContext == null)
//            {
//                throw new ArgumentNullException(nameof(bindingContext));
//            }

            
//            var _a = bindingContext.ValueProvider.GetValue("a");
//            var _b = bindingContext.ValueProvider.GetValue("b");

            
//            if (_a == ValueProviderResult.None || _b == ValueProviderResult.None)
//                return fallbackBinder.BindModelAsync(bindingContext);

            
//            string p1 = _a.FirstValue;
//            string p2 = _b.FirstValue;


//            if (!int.TryParse(p1, out var parsedP1) || !int.TryParse(p2, out var parsedP2))
//            {
//                throw new StackOverflowException();
//            }           
           


//            var result = new Fraction(parsedP1, parsedP2);

//            bindingContext.Result = ModelBindingResult.Success(result);
//            return Task.CompletedTask;
//        }
//    }
//}
