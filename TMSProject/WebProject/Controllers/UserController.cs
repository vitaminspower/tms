﻿using BLL.interfaces;
using DAL.Model;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;

namespace WebProject.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        public UserController(IUserService userService, IAccountService accountService)
        {
            _userService = userService;
            _accountService = accountService;
        }

        [HttpGet]
        [Authorize]
        [Route("/User/GetAllClients/")]
        public IActionResult GetAllClients()
        {
            var users = _userService.GetAll().ToList();
            //users.Reverse();

            return View(users);
        }

        [HttpGet]
        public IActionResult EditData(int id)
        {
            var Model = new User();

            Model = _userService.Get(id);

            var user = Model;

            return View(user);

        }


        [HttpGet]
        public IActionResult DeletePage(int id)
        {
            var Model = new User();

            Model = _userService.Get(id);

            var user = Model;

            return View(id);

        }

        [HttpPost]
        public IActionResult DeleteUser(int id)
        {
            var userToDelete = _userService.Get(id);
            _userService.Delete(userToDelete);

            return Redirect(Url.Action("GetAllClients", "User"));
        }


        [HttpPost]
        public IActionResult EditUserData(User newUser)
        {
            var oldUser = _userService.Get(newUser.Id);
            if (ModelState.IsValid)
            {
                oldUser.Name = newUser.Name;
                oldUser.Surname = newUser.Surname;

                oldUser.EmailAdress = newUser.EmailAdress;
                oldUser.PhoneNumber = newUser.PhoneNumber;

                oldUser.IsVerified = newUser.IsVerified;

                _userService.Update(oldUser);

                return Redirect(Url.Action("GetAllClients", "User"));

            }

            return Redirect(Url.Action("Index", "Home"));

        }

        public IActionResult AddNewClientPage()
        {
            return View();
        }

        public IActionResult CreateNew(User user)
        {
            _userService.Add(user);

            _accountService.AddDefault(user.Id);

            return Redirect(Url.Action("GetAllClients", "User")); ;
        }

    }
}
