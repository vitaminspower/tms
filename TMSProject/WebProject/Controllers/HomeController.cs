﻿using BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebProject.Extensions;
using WebProject.Models;

namespace WebProject.Controllers
{
    public class HomeController : Controller
    {

        private readonly IUserService _userService;
        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        #region old add methods

        //[HttpPost]
        //public string AddClient()
        //{

        //    for (int i = 0; i < 200; i++)
        //    {
        //        try
        //        {
        //            var user = new User();

        //            user.Name = "Name nr " + i.ToString();
        //            user.Surname = "Surname nr " + i.ToString();

        //            user.HomeAdress = "Empty street " + i.ToString();

        //            user.IsVerified = true;

        //            user.EmailAdress = "email@email.com";

        //            Random rnd = new Random();

        //            var rndph = string.Empty;

        //            for (int j = 0; i < 9; i++)
        //            {
        //                rndph += rnd.Next(0, 9).ToString();
        //            }

        //            user.PhoneNumber = rndph;

        //            user.RegistrationTime = DateTime.UtcNow;

        //            DateTime start = new DateTime(1995, 1, 1);
        //            int range = (DateTime.Today - start).Days;
        //            start.AddDays(rnd.Next(range));

        //            user.DateOfBirth = start;

        //            _context.Set<User>().Add(user);
        //        }
        //        catch (Exception e)
        //        {
        //            return e.InnerException.Message;
        //        }

        //    }
        //    _context.SaveChanges();

        //    return "done";
        //}

        //[HttpPost]
        //public string AddEmployee()
        //{
        //    for (int i = 0; i < 50; i++)
        //    {
        //        try
        //        {
        //            var employee = new Employee();

        //            employee.Name = "Name nr " + i.ToString();
        //            employee.Surname = "Surname nr " + i.ToString();

        //            employee.HomeAdress = "Empty street " + i.ToString();

        //            employee.EmailAdress = "private@email.com";

        //            employee.PhoneNumber = "123456789";
        //            employee.WorkPhoneNumber = "987654321";

        //            employee.HiredAt = DateTime.UtcNow;

        //            employee.DateOfBirth = new DateTime(2000, 1, 1);

        //            Random rnd = new Random();

        //            employee.BankOfficeId = rnd.Next(1, 3);
        //            employee.RoleId = rnd.Next(1, 3); 

        //            employee.HasContractTo = new DateTime(2012, 12, 12);

        //            _context.Set<Employee>().Add(employee);
        //        }
        //        catch (Exception e)
        //        {
        //            return e.InnerException.Message;
        //        }

        //    }
        //    _context.SaveChanges();

        //    return "done";
        //}

        //[HttpGet]
        //[Route("/Home/GetAllEmployees/{id}")]
        //public IActionResult GetAllEmployees([FromRoute] int id)
        //{
        //    var employees = new List<Employee>();

        //    employees = _context.Set<Employee>().ToList();

        //    return View(employees);
        //}

        #endregion

    }

}
