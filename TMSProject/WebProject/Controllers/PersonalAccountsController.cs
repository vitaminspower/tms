﻿using BLL.interfaces;
using DAL.Model;
using WebProject.Models.dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace WebProject.Controllers
{
    public class PersonalAccountsController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ITransactionService _transactionService;
        public PersonalAccountsController(IAccountService accountService, ITransactionService transactionService)
        {
            _accountService = accountService;
            _transactionService = transactionService;
        }
        //take id from claim

        //
        public IActionResult GetAllUserAccs(int id)
        {
            var model = new List<UserPersonalAccount>();

            model = _accountService.GetAll().Where(x => x.UserId == id).ToList();

            foreach (var acc in model)
            {
                acc.Amount = _transactionService.GetAmount(acc.Id);
            }

            return View(model);
        }


        [HttpGet]
        public IActionResult AddNewAccountPage(PersonalAccountDto acc)
        {
            return View(acc);
        }


        [HttpPost]
        public IActionResult AddNewAccount(PersonalAccountDto acc)
        {
            var newAcc = new UserPersonalAccount();

            newAcc.UserId = acc.UserId;

            _accountService.Add(newAcc);

            return Redirect($"GetAllUserAccs/{acc.UserId}");
        }


    }
}
