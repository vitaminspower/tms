﻿using BLL.interfaces;
using DAL.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebProject.Models.dto;

namespace WebProject.Controllers
{
    public class TransactionsController : Controller
    {
        private readonly ITransactionService _transactionService;
        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        public IActionResult AddTransaction(TransactionDto trDto)
        {
            return View(trDto);
        }

        [HttpPost]
        public IActionResult Add(TransactionDto tr)
        {
            _transactionService.Add(tr);

            return RedirectToRoutePermanent(new { controller = "User", action = "GetAllClients" });
        }

        [HttpGet]
        public IActionResult GetAccountIncomingTransactionsHistory(int id)
        {
            var model = _transactionService.GetAllIncomingTransactions(id);

            return View(model);
        }

        [HttpGet]
        public IActionResult GetAccountOutcomingTransactionsHistory(int id)
        {
            var model = _transactionService.GetAllOutcomingTransactions(id);

            return View(model);
        }


        public decimal GetAmount(int id)
        {
            var incTr = _transactionService.GetAllIncomingTransactions(id);
            var outcTr = _transactionService.GetAllOutcomingTransactions(id);

            return incTr.Sum(t => t.Amount) - outcTr.Sum(t => t.Amount);
        }
    }
}
