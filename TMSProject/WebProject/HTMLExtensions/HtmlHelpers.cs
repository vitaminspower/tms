﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using DAL.Model;

namespace WebProject.HTMLExtensions
{
    public static class HtmlHelpers
    {
        public static PropertyInfo GetPropertyInfo<TSource, TProperty>(
        this Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression member = propertyLambda.Body as MemberExpression;
            if (member is null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyLambda.ToString()));

            PropertyInfo propInfo = member.Member as PropertyInfo;
            if (propInfo is null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyLambda.ToString()));

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a property that is not from type {1}.",
                    propertyLambda.ToString(),
                    type));

            return propInfo;
        }

        public static IHtmlContent GetImageWithHint(this IHtmlHelper helper, string hintText, string width)
        {
            var hint = new TagBuilder("img");

            hint.Attributes.Add("type", "image");
            hint.Attributes.Add("title", hintText);
            hint.Attributes.Add("src", "https://images.pexels.com/photos/259027/pexels-photo-259027.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");
            hint.Attributes.Add("width", width);


            return hint;
        }



        public static IHtmlContent GetHintBox<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var hint = new TagBuilder("div");

            hint.Attributes.Add("name", "button");

            hint.InnerHtml.AppendHtml(helper.TextBoxFor(expression, htmlAttributes));

            var prop = expression.GetPropertyInfo();

            var attribute = prop.GetCustomAttribute<HintAttribute>();

            var imgHint = GetImageWithHint(helper, attribute.Text, "25");

            hint.InnerHtml.AppendHtml(imgHint);

            return hint;
        }

       

    }
}
