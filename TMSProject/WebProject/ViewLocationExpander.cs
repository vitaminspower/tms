﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject
{
    public class CustomViewLocationExpander : IViewLocationExpander
    {
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {            
            List<string> expandedViewLocations = new List<string>();

            expandedViewLocations.Add("/Views/AlternateFolder/{1}/{0}.cshtml");

            expandedViewLocations.AddRange(viewLocations);

            return expandedViewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
        }
    }
}
