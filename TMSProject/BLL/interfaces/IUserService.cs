﻿using DAL.Model;
using System.Linq;

namespace BLL.interfaces
{
    public interface IUserService
    {
        IQueryable<User> GetAll();
        User Get(int id);
        User Update(User user);
        void Add(User user);
        void Delete(User user);
    }
}
