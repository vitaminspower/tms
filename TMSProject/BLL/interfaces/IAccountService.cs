﻿using DAL.Model;
using System.Linq;


namespace BLL.interfaces
{
    public interface IAccountService
    {
        UserPersonalAccount Get(int id);
        IQueryable<UserPersonalAccount> GetAll();
        UserPersonalAccount Update(UserPersonalAccount acc);
        void Add(UserPersonalAccount acc);
        void Delete(UserPersonalAccount acc);
        void AddDefault(int id);

    }
}
