﻿using DAL.Model;
using System.Collections.Generic;
using WebProject.Models.dto;

namespace BLL.interfaces
{
    public interface ITransactionService
    {
        void Add(TransactionDto transactionDto);
        List<IncomingTransaction> GetAllIncomingTransactions(int id);
        List<OutcomingTransaction> GetAllOutcomingTransactions(int id);

        decimal GetAmount(int id);
    }
}
