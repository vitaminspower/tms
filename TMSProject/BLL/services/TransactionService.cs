﻿using BLL.interfaces;
using DAL;
using DAL.Model;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using WebProject.Models.dto;


namespace BLL.services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _uow;
        public TransactionService(IUnitOfWork uow)
        {
            _uow = uow;

        }

        public void Add(TransactionDto tr)
        {
            SqlTransaction transaction; 

           //BeginTransaction w/0 sqlconnection

            var outcomingTransaction = new OutcomingTransaction();
            var incomingTransaction = new IncomingTransaction();


            outcomingTransaction.Amount = -tr.Amount;
            outcomingTransaction.Source = "Bank office";
            outcomingTransaction.SenderBankAccountId = tr.SenderId;
            outcomingTransaction.ReceiverBankAccountId = tr.ReciverId;

            incomingTransaction.Amount = tr.Amount;
            incomingTransaction.Source = "Bank office";
            incomingTransaction.SenderBankAccountId = tr.SenderId;
            incomingTransaction.ReceiverBankAccountId = tr.ReciverId;

            _uow.OutcomingTransactions.Add(outcomingTransaction);
            _uow.IncomingTransactions.Add(incomingTransaction);

            _uow.SaveChanges();
        }

        public List<IncomingTransaction> GetAllIncomingTransactions(int id)
        {
            return _uow.IncomingTransactions.ReadAll().Where(x => x.ReceiverBankAccountId == id).ToList();
        }

        public List<OutcomingTransaction> GetAllOutcomingTransactions(int id)
        {
            return _uow.OutcomingTransactions.ReadAll().Where(x => x.SenderBankAccountId == id).ToList();
        }

        public decimal GetAmount(int accId)
        {
            var incTr = GetAllIncomingTransactions(accId);
            var outcTr = GetAllOutcomingTransactions(accId);

            return outcTr.Sum(t => t.Amount) + incTr.Sum(t => t.Amount);
        }

    }
}

