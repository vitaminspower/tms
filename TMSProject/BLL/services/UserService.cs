﻿using BLL.interfaces;
using DAL;
using DAL.Model;
using System.Linq;

namespace BLL.services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IQueryable<User> GetAll()
        {
            return
             _uow.Users
                .ReadAll();
        }

        public User Get(int id)
        {
            return
              _uow.Users.Read(id);
        }

        public void Delete(User user)
        {
            _uow.Users.Delete(user);
            _uow.SaveChanges();
        }

        public User Update(User update)
        {
            var updated = _uow.Users.Update(update);
            _uow.SaveChanges();
            return updated;
        }

        public void Add(User user)
        {
            _uow.Users.Add(user);

            _uow.SaveChanges();
        }
    }
}
