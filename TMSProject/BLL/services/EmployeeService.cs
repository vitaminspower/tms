﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.interfaces;
using DAL;
using DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace BLL.services
{
    public class EmployeeService : IEmployeeService
    {
        private IUnitOfWork _uow;
        public EmployeeService(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        public Employee Get(string login, string password)
        {
            return _uow.Employee.ReadAll().FirstOrDefault(x => x.Login == login && x.Password == password);
        }
    }
}
