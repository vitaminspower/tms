﻿using BLL.interfaces;
using DAL;
using DAL.Model;
using System.Linq;

namespace BLL.services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _uow;
        public AccountService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IQueryable<UserPersonalAccount> GetAll()
        {
            return
             _uow.Accounts
                .ReadAll();
        }

        public UserPersonalAccount Get(int id)
        {
            return
              _uow.Accounts.Read(id);
        }

        public void Delete(UserPersonalAccount acc)
        {
            _uow.Accounts.Delete(acc);
            _uow.SaveChanges();
        }

        public UserPersonalAccount Update(UserPersonalAccount update)
        {
            var updated = _uow.Accounts.Update(update);
            _uow.SaveChanges();
            return updated;
        }

        public void Add(UserPersonalAccount acc)
        {
            _uow.Accounts.Add(acc);
            _uow.SaveChanges();
        }

        public void AddDefault(int id)
        {
            var acc = new UserPersonalAccount();

            acc.UserId = id;
            //acc.Amount = 0;

            _uow.Accounts.Add(acc);
            _uow.SaveChanges();
        }
    }
}
