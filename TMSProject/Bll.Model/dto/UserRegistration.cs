﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebProject.Models.dto
{
    public class UserRegistration //: IValidatableObject
    {
        #region unused for now
        
        [Required]
        [MinLength(1, ErrorMessage = "min lenght error")]
        [MaxLength(64, ErrorMessage = "max lenght error")]
        public string Login { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [MinLength(4, ErrorMessage = "min lenght error")]
        [MaxLength(64, ErrorMessage = "max lenght error")]
        public string Password { get; set; }

        [Required]
        [MinLength(4, ErrorMessage = "min lenght error")]
        [MaxLength(64, ErrorMessage = "max lenght error")]
        public string SecondPassword { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var result = new List<ValidationResult>();

            if (!string.Equals(Password, SecondPassword, StringComparison.Ordinal))
            {
                result.Add(new ValidationResult("not equal passwords",
                    new[] { nameof(Password), nameof(SecondPassword) }));

            }

            return result;

        }

        #endregion
    }

}
