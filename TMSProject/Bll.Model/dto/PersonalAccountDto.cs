﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebProject.Models.dto
{
    public class PersonalAccountDto
    {
        public int UserId { get; set; }
    }
}
