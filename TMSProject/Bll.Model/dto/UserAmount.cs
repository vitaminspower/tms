﻿namespace WebProject.Models.dto
{
    public class UserAmount
    {
        public int UserId { get; set; }

        public string NameAndSurname { get; set; } //if seen via mobile app or widget

        public long Amount { get; set; }
    }
}
