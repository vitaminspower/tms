﻿using System.ComponentModel.DataAnnotations;

namespace WebProject.Models.dto
{
    public class UserAut
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
