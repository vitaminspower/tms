﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebProject.Models.dto
{
   public class TransactionDto
    {
        public int SenderId { get; set; } = 0;
        public int ReciverId { get; set; }

        public int SenderCurrencyId { get; set; }
        public int RecivierCurrencyId { get; set; }

        public decimal Amount { get; set; }

    }
}
