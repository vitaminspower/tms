﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DAL.Model.Enum;

namespace DAL.Model
{
    public class UserPersonalAccount
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        [NotMapped]
        public decimal Amount { get; set; }
        //    {
        //            return IncomingTransactions.Sum(t => t.Amount) - OutComingTransactions.Sum(t => t.Amount);

        //    }
        //}

        public Currency Currency { get; set; } = Currency.USD;

        [InverseProperty("ReceiverAccount")]
        public virtual ICollection<IncomingTransaction> IncomingTransactions { get; set; }

        [InverseProperty("SenderAccount")]
        public virtual ICollection<OutcomingTransaction> OutComingTransactions { get; set; }


    }
}
