﻿using DAL.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Model
{
    public class IncomingTransaction
    {
        [Required, Key]
        public int Id { get; set; }

        [Required]
        public int ReceiverBankAccountId { get; set; }

        public int SenderBankAccountId { get; set; }

        public string Source { get; set; }

        public Currency Currenct { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public DateTime Date { get; set; } //generated as GetDate via fluent api 

        [ForeignKey("SenderBankAccountId")]
        public virtual UserPersonalAccount SenderAccount { get; set; }

        [ForeignKey("ReceiverBankAccountId")]
        public virtual UserPersonalAccount ReceiverAccount { get; set; }

    }
}
