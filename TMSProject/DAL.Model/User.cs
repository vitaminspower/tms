﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Model
{
    public class User
    {
        [Required, Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "invalid lenght")]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "you can only use eng letters")]
        [Hint("enter your name")]
        public string Name { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "invalid lenght")]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "you can only use eng letters")]
        public string Surname { get; set; }

        [MaxLength(50)]
        public string HomeAdress { get; set; }

        [Required(ErrorMessage = "error")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "this fiels is required")]
        public string EmailAdress { get; set; }

        [Required(ErrorMessage = "Required")]
        public DateTime DateOfBirth { get; set; }

        public DateTime RegistrationTime { get; set; } //context generation

        [Required]
        public bool IsVerified { get; set; }  //context generation to false

        public virtual ICollection<OutcomingTransaction> Transactions { get; set; }

        public virtual ICollection<UserPersonalAccount> Accounts { get; set; }

    }
}
