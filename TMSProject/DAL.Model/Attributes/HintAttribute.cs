﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class HintAttribute : Attribute
    {
        public string Text { get; private set; }
        public HintAttribute(string value)
        {
            Text = value;
        }
    }
}
