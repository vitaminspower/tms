﻿namespace DAL.Model.Enum
{
    public enum Operations
    {
        AcceptanceOfDeposits, //plus
        LendingOfFunds,       //plus+rate
        ClearingOfCheques,    //minus
        RemittanceOfFunds,    //minus+plus
        LockersSafeDeposits,  //resaving
        BillPaymentServices,  //minus
        OnlineBanking,         //--
        CreditDebitCards,     //plus or minus
        OverseasBankingServices, //plus and minus
        WealthManagement,      //--
        InvestmentBanking,     //TODO
        SocialObjectives       //--
    }
}
