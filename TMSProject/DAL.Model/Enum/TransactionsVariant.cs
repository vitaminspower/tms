﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Model.Enum
{
    public enum TransactionsVariant
    { //comissions below
        HomeBank,                //0
        HomeATM,                 //0
        AllyBank,                //0.1
        AllyATM,                 //0.1
        OtherBanksOrBankomats,   //0.5
        ForeignBankOrATM,        //1.5
        MobileApp,               //0
        WebApp,                  //0
        CardTransaction          //0
    }

   
}
