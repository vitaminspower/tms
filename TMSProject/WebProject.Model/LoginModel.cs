﻿using System.ComponentModel.DataAnnotations;

namespace WebProject.Model
{
    public class LoginModel
    {
        [Required(ErrorMessage = "this field is required")]
        public string Login { get; set; }

        [Required(ErrorMessage = "this field is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
