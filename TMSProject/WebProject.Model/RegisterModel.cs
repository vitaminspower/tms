﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebProject.Model
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "this field is required")]
        public string Login { get; set; }

        [Required(ErrorMessage = "this field is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "not matching")]
        public string ConfirmPassword { get; set; }
    }
}
