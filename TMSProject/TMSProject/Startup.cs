﻿// dotnet ef migrations add first_entities --startup-project TMSProject --project TMSProject --verbose
// dotnet ef database update --startup-project TMSProject --project TMSProject --verbose
//@"Data Source=.\SQLEXPRESS;Initial Catalog=MainDb;Integrated Security=True"

namespace DAL
{
    public class Startup
    {
        public Startup()
        {
            #region old DI
            //    var config = new ConfigurationBuilder()
            //         .SetBasePath(Directory.GetCurrentDirectory())
            //         .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
            //         .Build();

            //    var connectionString = config.GetConnectionString("DefaultConnection");

            //    var service = new ServiceCollection()
            //       .AddDbContext<DbContext, MainDBContext>(x => x.UseSqlServer(connectionString))
            //       .AddTransient<BankManager>()      
            //       .BuildServiceProvider();

            //    var _service = service.GetService<BankManager>();

            #endregion

            #region thread samples

            //var t1 = new Thread(Print);
            //var t2 = new Thread(Print);

            //t1.Start();
            //t2.Start();

            // Task.Run(() => Method());



            //for (int i = 0; i < 100; i++)
            //{
            //    Task.Run(() => process());
            //}

            //{
            //    var cassa = pool.get();
            //    Console.WriteLine("client 1na kasse 1");
            //    Thread.Sleep(rnd.Next(1, 2000));
            //}


            //ConcurrentDictionary<key, value> list = new System.Collections.Concurrent.ConcurrentDictionary();

            #endregion

        }

        #region async samples

        //public static void Print()
        //{
        //    //  var str = string.Empty;
        //    var str = "123456789";

        //    lock (sync)
        //    {
        //        foreach (var item in str)
        //        {
        //            Console.Write(item);
        //            Thread.Sleep(250);
        //        }

        //        Console.WriteLine("");
        //    }        

        //}

        //public static async Task GetString()
        //{
        //    Console.WriteLine("get string");
        //}

        //public async Task<string> Hello()
        //{
        //    await GetString();
        //    Console.WriteLine("");

        //    return "";

        //}

        #endregion
    }
}
